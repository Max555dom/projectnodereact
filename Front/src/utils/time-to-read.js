// 40 words === 1 min
export function timeToRead(str) {
  return Math.floor(str.split(' ').length / 120);
}
