import createDataContext from './createDataContext';
import bookApi from '../api/book';
import { navigate } from '@reach/router';

const authReducer = (state, action) => {
  switch (action.type) {
    case 'fetch_user':
      const {email, pseudo } = action.payload;
      return { ...state, user: {email, pseudo } };
    case 'signin':
      return { token: action.payload, errorMessage: '' };
    case 'signout':
      return { token: null, errorMessage: '', pseudo: '' };
    case 'error':
      return { ...state, errorMessage: action.payload };
    default:
      return state;
  }
};

const tryLocalSignin = dispatch => () => {
  const token = window.localStorage.getItem('token');
  if (token) {
    dispatch({ type: 'signin', payload: token }); 
  } else {
    return null;
  }
};

const fetchUser = dispatch => async () => {
  try {
    const {
      data: { email , pseudo }
    } = await bookApi.get('/');
    dispatch({ type: 'fetch_user', payload: {email, pseudo } });
  } catch (err) {
    console.log(err);
  }
};


const signup = dispatch => async ({ firstname, lastname, pseudo, email, password }) => {
  try {
    const response = await bookApi.post('/users', {
      firstname,
      lastname,
      pseudo,
      email,
      password
    });
    const { token } = response.data;
   await window.localStorage.setItem('token', token);
    dispatch({ type: 'signin', payload: token });
    navigate('/');
  } catch (err) {
    dispatch({
      type: 'error',
      payload: `Une erreur s'est produite pendant l'inscription : ${err}`
    });
  }
};



const signin = dispatch => async ({ email, password }) => {
  try {
   const response = await bookApi.post('/users/auth', { email, password });
   await window.localStorage.setItem('token', response.data.token);
   dispatch({ type: 'signin', payload: response.data.token });
   navigate('/');
  } catch (err) {
    dispatch({ type: 'error', payload: 'Une erreur s\'est produite pendant la connexion : ' });
  }
};

const signout = dispatch => async () => {
  await window.localStorage.removeItem('token');
  dispatch({ type: 'signout' });
  navigate('/');
};

export const { Provider, Context } = createDataContext(
  authReducer,
  { tryLocalSignin, fetchUser, signup, signin, signout },
  { token: null, user: {}, errorMessage: '' }
);
