import createDataContext from './createDataContext';
import bookApi from '../api/book';
import { navigate } from '@reach/router';
const kindReducer = (state, action) => {
  switch (action.type) {
    case 'fetch_kinds':
      return action.payload;
    case 'update_kind':
      return state.map(kind => {
        const { id, libelle } = action.payload;
        return kind._id === id
          ? {
              ...kind,
              libelle,
            }
          : kind;
      });
    case 'delete_kind':
      return state.filter(kind => kind._id !== action.payload);
    default:
      return state;
  }
};

const fetchKinds = dispatch => async () => {
  try {
    const response = await bookApi.get('/genres');
    dispatch({ type: 'fetch_kinds', payload: response.data });
  } catch (err) {
    console.log(err);
  }
};

const createKind = dispatch => async ({ libelle }) => {
  try {
    await bookApi.post('/genres', { libelle });
    navigate('/');
  } catch (err) {
    console.log(err);
  }
};

const updateKind = dispatch => async ({ id, libelle }) => {
  dispatch({ type: 'update_kind', payload: { id, libelle } });
  window.history.back();
  try {
    await bookApi.put('/genres', { id, libelle });
  } catch (err) {
    console.log('FRONTEND', err);
  }
};

const deleteKind = dispatch => async id => {
  dispatch({ type: 'delete_kind', payload: id });
  try {
    await bookApi.delete(`/genres/${id}`);
  } catch (err) {
    console.log('Frontend', err);
  }
};

export const { Provider, Context } = createDataContext(
  kindReducer,
  { fetchKinds, createKind, updateKind, deleteKind },
  []
);
