import createDataContext from './createDataContext';
import bookApi from '../api/book';
import { navigate } from '@reach/router';
const bookReducer = (state, action) => {
  switch (action.type) {
    case 'fetch_books':
      return action.payload;
    case 'update_book':
      return state.map(book => {
        const { id, title, auteur, genre, description, nbrPage, category, link, thumbnail } = action.payload;
        return book._id === id
          ? {
              ...book,
              title,
              auteur,
              genre,
              description,
              nbrPage,
              category,
              link,
              thumbnail
            }
          : book;
      });
    case 'delete_book':
      return state.filter(book => book._id !== action.payload);
    default:
      return state;
  }
};



const fetchBooks = dispatch => async () => {
  try {
    const response = await bookApi.get('/book');
    dispatch({ type: 'fetch_books', payload: response.data });
  } catch (err) { 
    console.log(err);
  }
};

const createBook = dispatch => async ({ title, auteur, genre, description, nbrPage, link, thumbnail }) => {
  try {
    await bookApi.post('/book', {  title, auteur, genre, description, nbrPage, link,  thumbnail });
    navigate('/');
  } catch (err) {
    console.log(err);
  }
};

const updateBook = dispatch => async ({ id, title, auteur, genre, description, nbrPage, link, thumbnail }) => {
  dispatch({ type: 'update_book', payload: { id, title, auteur, genre,  description, nbrPage, link, thumbnail} });
  window.history.back();
  try {
    await bookApi.put(`/book/${id}`, { id, title, auteur, genre, description, nbrPage, link, thumbnail });
  } catch (err) {
    console.log('FRONTEND', err);
  }
};

const deleteBook = dispatch => async id => {
  dispatch({ type: 'delete_book', payload: id });
  try {
    await bookApi.delete(`/book/${id}`);
  } catch (err) {
    console.log('Frontend', err);
  }
};

export const { Provider, Context } = createDataContext(
  bookReducer,
  { fetchBooks, createBook, updateBook, deleteBook },
  []
);
