import createDataContext from './createDataContext';
import bookApi from '../api/book';
import { navigate } from '@reach/router';
const catReducer = (state, action) => {
  switch (action.type) {
    case 'fetch_categories':
      return action.payload;
    case 'update_category':
      return state.map(category => {
        const { id, libelle } = action.payload;
        return category._id === id
          ? {
              ...category,
              libelle,
            }
          : category;
      });
    case 'delete_category':
      return state.filter(category => category._id !== action.payload);
    default:
      return state;
  }
};

const fetchCategories = dispatch => async () => {
  try {
    const response = await bookApi.get('/category');
    dispatch({ type: 'fetch_categories', payload: response.data });
  } catch (err) {
    console.log(err);
  }
};

const createCategory = dispatch => async ({ libelle }) => {
  try {
    await bookApi.post('/category', { libelle });
    navigate('/');
  } catch (err) {
    console.log(err);
  }
};

const updateCategory = dispatch => async ({ id, libelle }) => {
  dispatch({ type: 'update_category', payload: { id, libelle } });
  window.history.back();
  try {
    await bookApi.put('/category', { id, libelle });
  } catch (err) {
    console.log('FRONTEND', err);
  }
};

const deleteCategory = dispatch => async id => {
  dispatch({ type: 'delete_category', payload: id });
  try {
    await bookApi.delete(`/category/${id}`);
  } catch (err) {
    console.log('Frontend', err);
  }
};

export const { Provider, Context } = createDataContext(
  catReducer,
  { fetchCategories, createCategory, updateCategory, deleteCategory },
  []
);
