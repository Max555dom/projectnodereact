import React, { Suspense, lazy, useContext, useEffect } from 'react';
import { Context as BookContext } from '../context/BookContext';
import { Text } from '../styles';
import Spinner from '../components/Spinner';
const BookDetail = lazy(() => import('../components/BookDetail'));

const IndexScreen = () => {
  const { state, fetchBooks, deleteBook } = useContext(BookContext);

  useEffect(() => {
    fetchBooks();
  }, []);

  return (
    <div style={{ marginTop: '20px' }}>
      {state.length === 0 && (
        <div>
          <Text>Vous n'avez aucun livres pour le moment.</Text>
        </div>
      )}
      {state.length > 0 && <h2>Mes livres.</h2>}
      {state.map(book => (
        <Suspense key={book._id} fallback={<Spinner />}>
          <BookDetail {...book} onDelete={deleteBook} />
        </Suspense>
      ))}
    </div>
  );
};
export default IndexScreen;
