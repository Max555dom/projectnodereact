import React, { useContext } from 'react';
import KindForm from '../components/KindForm';
import { Context as KindContext } from '../context/KindContext';

const CreateKindScreen = () => {
  const { createKind } = useContext(KindContext);
  return (
    <div>
      <h3>Ajouter un genre</h3>
      <KindForm onSubmit={createKind} text="Ajouter un genre" />
    </div>
  );
};
export default CreateKindScreen;
