import React, { useContext, useEffect } from 'react';
import { Context as BookContext } from '../context/BookContext';
import BookForm from '../components/BookForm';

const EditScreen = ({ location }) => {
  const { state, fetchBooks, updateBook } = useContext(BookContext);
  const { id } = location.state;

  useEffect(() => {
    fetchBooks();
  }, []);

  const book = state.find(b => b._id === id);

  if (!book) {
    return 'Veuillew patienter...';
  }

  return (
    <div>
      <BookForm
        onSubmit={({ title, auteur, genre, description, nbrPage, thumbnail, link }) =>
          updateBook({ id, title, auteur, genre, description, nbrPage, thumbnail, link })
        }
        initialValues={{
          title: book.title,
          auteur: book.auteur,
          description: book.description,
          nbrPage: book.nbrPage,
          genre: book.genre,
          category: book.category,
          thumbnail: book.thumbnail,
          link: book.link
        }}
        text="Confirm changes"
        editing
      />
    </div>
  );
};
export default EditScreen;
