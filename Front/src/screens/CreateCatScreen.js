import React, { useContext } from 'react';
import CategoryForm from '../components/CategoryForm';
import { Context as CatContext } from '../context/CatContext';

const CreateCatScreen = () => {
  const { createCategory } = useContext(CatContext);
  return (
    <div>
      <h3>Ajouter une catégorie</h3>
      <CategoryForm onSubmit={createCategory} text="Ajouter une catégorie" />
    </div>
  );
};
export default CreateCatScreen;
