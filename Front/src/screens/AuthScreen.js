import React, { useState, useContext } from 'react';
import AuthForm from '../components/AuthForm';
import { Context as AuthContext } from '../context/AuthContext';
import { AuthWrapper, Instead } from '../styles/index';

const AuthScreen = () => {
  const { state, signup, signin } = useContext(AuthContext);

  // I need to show the signin form for the first time
  const [showSignup, setShowSignup] = useState(false);

  return (
    <AuthWrapper>
      {showSignup ? (
        <>
          <AuthForm
            title="Inscription"
            subtitle="Créer un compte en remplissant tous les champs ci-dessous : "
            onSubmit={signup}
            errorMessage={state.errorMessage}
          />
          <Instead onClick={() => setShowSignup(false)}>
            Vous avez déjà un compte ? Connectez-vous maintenant !
          </Instead>
        </>
      ) : (
        <>
          <AuthForm
            title="Se connecter"
            subtitle="Connectez-vous à votre compte en remlissant l'email et le mot de passe uniquement : "
            onSubmit={signin}
            errorMessage={state.errorMessage}
          />
          <Instead onClick={() => setShowSignup(true)}>
           Pas encore de compte ? Inscrivez-vous maintenant 
          </Instead>
        </>
      )}
    </AuthWrapper>
  );
};
export default AuthScreen;
