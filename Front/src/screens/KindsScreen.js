import React, { Suspense, lazy, useContext, useEffect } from 'react';
import { Context as KindContext } from '../context/KindContext';
import { Text } from '../styles';
import Spinner from '../components/Spinner';
const KindDetail = lazy(() => import('../components/KindDetail'));

const KindsScreen = () => {
  const { state, fetchKinds, deleteKind } = useContext(KindContext);

  useEffect(() => {
    fetchKinds();
  }, []);

  return (
    <div style={{ marginTop: '20px' }}>
      {state.length === 0 && (
        <div>
          <Text>Vous n'avez aucun genre pour le moment.</Text>
        </div>
      )}
      {state.length > 0 && <h2>Mes Genres.</h2>}
      {state.map(kind => (
        <Suspense key={kind._id} fallback={<Spinner />}>
          <KindDetail {...kind} onDelete={deleteKind} />
        </Suspense>
      ))}
    </div>
  );
};
export default KindsScreen;
