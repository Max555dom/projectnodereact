import React, { useContext, useEffect } from 'react';
import { Context as KindContext } from '../context/KindContext';
import KindForm from '../components/KindForm';

const EditKindScreen = ({ location }) => {
  const { state, fetchKinds, updateKind } = useContext(KindContext);
  const { id } = location.state;

  useEffect(() => {
    fetchKinds();
  }, []);

  const kind = state.find(b => b._id === id);

  if (!kind) {
    return 'Veuillew patienter...';
  }

  return (
    <div>
      <KindForm
        onSubmit={({ libelle }) =>
          updateKind({ id, libelle })
        }
        initialValues={{
          libelle: kind.libelle,
        }}
        text="Confirm changes"
        editing
      />
    </div>
  );
};
export default EditKindScreen;
