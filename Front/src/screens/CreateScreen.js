import React, { useContext } from 'react';
import BookForm from '../components/BookForm';
import { Context as BookContext } from '../context/BookContext';

const CreateScreen = () => {
  const { createBook } = useContext(BookContext);
  return (
    <div>
      <h3>Ajouter un livre</h3>
      <BookForm onSubmit={createBook} text="Ajouter un livre" />
    </div>
  );
};
export default CreateScreen;
