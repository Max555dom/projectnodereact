import React, { Suspense, lazy, useContext, useEffect } from 'react';
import { Context as CatContext } from '../context/CatContext';
import { Text } from '../styles';
import Spinner from '../components/Spinner';
const CatDetail = lazy(() => import('../components/CatDetail'));

const CategoryScreen = () => {
  const { state, fetchCategories, deleteCategory } = useContext(CatContext);

  useEffect(() => {
    fetchCategories();
  }, []);

  return (
    <div style={{ marginTop: '20px' }}>
      {state.length === 0 && (
        <div>
          <Text>Vous n'avez aucunes catégories pour le moment.</Text>
        </div>
      )}
      {state.length > 0 && <h2>Mes Catégories.</h2>}
      {state.map(category => (
        <Suspense key={category._id} fallback={<Spinner />}>
          <CatDetail {...category} onDelete={deleteCategory} />
        </Suspense>
      ))}
    </div>
  );
};
export default CategoryScreen;
