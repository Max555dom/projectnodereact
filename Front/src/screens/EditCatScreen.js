import React, { useContext, useEffect } from 'react';
import { Context as CatContext } from '../context/CatContext';
import CategoryForm from '../components/CategoryForm';

const EditCatScreen = ({ location }) => {
  const { state, fetchCategories, updateCategory } = useContext(CatContext);
  const { id } = location.state;

  useEffect(() => {
    fetchCategories();
  }, []);

  const category = state.find(b => b._id === id);

  if (!category) {
    return 'loading...';
  }

  return (
    <div>
      <CategoryForm
        onSubmit={({ libelle }) =>
          updateCategory({ id, libelle })
        }
        initialValues={{
          libelle: category.libelle,
        }}
        text="Confirm changes"
        editing
      />
    </div>
  );
};
export default EditCatScreen;
