import React, { Component, useContext, useEffect } from 'react';
import { Context as BookContext } from '../context/BookContext';
import { stringToUrl } from '../utils/string-to-url';
import { firstCapitalLetter } from '../utils/first-capital-letter';
import { Link } from '@reach/router';
import { Title, Text, Image, Edit } from '../styles/index';
import { FiEdit2 } from 'react-icons/fi';

const DetailScreen = ({ location }) => {
  const { state, fetchBooks } = useContext(BookContext);
  useEffect(() => {
    fetchBooks();
  }, []);

  const book = state.find(b => b._id === location.state.id);
  if (!book) {
    return <div>Veuillez patienter...</div>;
  }
  return (
    <div>
      <Title>{firstCapitalLetter(book.title)}</Title>
      <Image
        loading="lazy"
        width="100"
        height="auto"
        src={`http://localhost:3000/uploads/book/${book.thumbnail}`}
        alt={book.title}  
      />
      <Text>{book.content}</Text>
      <Link to={`/edit/${stringToUrl(book.title)}`} state={{ id: book._id }}>
        <Edit>
          <>
            <FiEdit2 style={{ marginRight: '10px' }} /> Modifier le livre
          </>
        </Edit>
      </Link>
    </div>
  );
};
export default DetailScreen;
