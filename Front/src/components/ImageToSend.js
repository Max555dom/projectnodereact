import React, { useState, useContext, useEffect } from 'react';
import { Context as CatContext } from '../context/CatContext';
import { Context as KindContext } from '../context/KindContext';
import ImageUploader from 'react-images-upload';

export default class ImageToSend extends React.Component {

    constructor(props) {
        super(props);
        this.state = { pictures: [] };
        this.onDrop = this.onDrop.bind(this);
    }

    onDrop(thumbnail, pictureDataURLs) {
        
        this.setState({
            pictures: thumbnail
        });
    }

    render() {
        return (
            <ImageUploader
                withIcon={true}
                buttonText='Choose images'
                onChange={this.onDrop}
                imgExtension={['.jpg', '.gif', '.png', '.gif']}
                maxFileSize={5242880}
            />
        );
    }
}