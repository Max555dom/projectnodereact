import React from 'react';
import { Content } from '../styles/index';

const UserProfile = ({email, pseudo }) => {
  return (
    <div>
      <Content>
        Bonjour : <strong>{pseudo}</strong>!
      </Content>
      <Content>
        Ton adresse email est : <strong>{email}</strong>
      </Content>
    </div>
  );
};
export default UserProfile;