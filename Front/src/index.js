import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider as AuthProvider } from './context/AuthContext';
import { Provider as BookProvider } from './context/BookContext';
import { Provider as CatProvider } from './context/CatContext';
import { Provider as KindProvider } from './context/KindContext';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <AuthProvider>
    <BookProvider>
    <CatProvider>
    <KindProvider>
      <App />
    </KindProvider>
      </CatProvider>
    </BookProvider>
  </AuthProvider>,
  document.getElementById('root')
);

serviceWorker.unregister();
